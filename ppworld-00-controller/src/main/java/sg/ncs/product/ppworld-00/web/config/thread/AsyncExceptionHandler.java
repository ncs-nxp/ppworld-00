package sg.ncs.product.ppworld-00.web.web.config.thread;

import sg.ncs.product.ppworld-00.web.common.utils.MailUtil;
import sg.ncs.product.ppworld-00.web.common.utils.SpringContextHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.interceptor.SimpleAsyncUncaughtExceptionHandler;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Method;

public class AsyncExceptionHandler extends SimpleAsyncUncaughtExceptionHandler {
    private static final Logger logger = LoggerFactory.getLogger(AsyncExceptionHandler.class);

    private static MailUtil mailUtil = null;

    @Override
    public void handleUncaughtException(Throwable ex, Method method, Object... params) {
        if(mailUtil == null) {
            mailUtil = SpringContextHolder.getBean(MailUtil.class);
        }
        StringWriter sw = new StringWriter();
        ex.printStackTrace(new PrintWriter(sw,true));
        logger.error(ex.getClass().getName() + " " + sw.toString(), this.getClass());
        mailUtil.sendErrorAsync("ppworld-00 Service exception", this.getClass().getCanonicalName() + System.getProperty("line.separator") + sw.toString());
    }
}
