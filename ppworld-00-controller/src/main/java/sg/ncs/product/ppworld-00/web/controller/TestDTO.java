package sg.ncs.product.ppworld-00.web.web.controller;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Data
public class TestDTO {

    @ApiModelProperty(value = "name")
    private String name;

    @NotNull
    @Size(min = 1,message = "typeList length must be greater than 0")
    @ApiModelProperty(value = "Array parameter")
    private List<String> typeList;

    @ApiModelProperty(value = "Date parameter yyyy-MM-dd")
    private LocalDate date;

    @ApiModelProperty(value = "Time parameter yyyy-MM-dd HH:mm:ss")
    private LocalDateTime time;

    @NotBlank
    @ApiModelProperty(value = "order number", required = true)
    private String orderNo;
}
